package com.iktpreobuka.dnevnik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DnevnikProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(DnevnikProjectApplication.class, args);
	}
}
