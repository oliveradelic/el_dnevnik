package com.iktpreobuka.dnevnik.controllers;


import org.springframework.beans.factory.annotation.Autowired;

/*import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;
	import org.springframework.http.HttpHeaders;
	import org.springframework.http.HttpStatus;
	import org.springframework.http.ResponseEntity;
	import org.springframework.web.bind.annotation.RequestBody;
	import org.springframework.web.util.UriComponentsBuilder;*/

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iktpreobuka.dnevnik.entities.KorisnikEntity;
import com.iktpreobuka.dnevnik.repositories.KorisnikRepository;

@RestController
@RequestMapping(path = "/api/v1/korisnik")
public class KorisnikController {

		  @Autowired
	      private KorisnikRepository korisnikRepository;
	      
	      @RequestMapping(method = RequestMethod.POST)
	      	public KorisnikEntity addNewUser(@RequestParam boolean jeAdmin, @RequestParam String korisnickoIme, 
	      			@RequestParam String password) {
	    	  
	    	  KorisnikEntity user = new KorisnikEntity();
	    	  user.setJeAdmin(jeAdmin);
	    	  user.setKorisnickoIme(korisnickoIme);
	    	  user.setPassword(password);
	    	  korisnikRepository.save(user);
	    	  return user;
	      }
	      
	      @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	  	public KorisnikEntity updateUser(@PathVariable Integer id, @RequestParam boolean jeAdmin,
	  			@RequestParam String korisnickoIme, 
	  			@RequestParam String password) {
	    	KorisnikEntity user = korisnikRepository.findOne(id);
	  		user.setJeAdmin(jeAdmin);
	  		user.setKorisnickoIme(korisnickoIme);
	  		user.setPassword(password);
	  		return korisnikRepository.save(user);
	  	}
	      
	      @RequestMapping(method = RequestMethod.GET)
	      	public Iterable<KorisnikEntity> getAllUsers() {
	    	  return korisnikRepository.findAll();
	      }
	      
	      @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	      	public void deleteUser(@PathVariable Integer id){
	    	  KorisnikEntity user = korisnikRepository.findOne(id);
	    	  korisnikRepository.delete(user);
	    			  
	      }
	}

	 
	/*@RestController
	@RequestMapping("/api")

	public class UcenikController {
		

		public static final Logger logger = LoggerFactory.getLogger(UcenikController.class);
		 
	    @Autowired
	    UcenikRepository ucenikRepository; //Service which will do all data retrieval/manipulation work
	 
	    // -------------------Retrieve All Users---------------------------------------------
	 
	    @RequestMapping(value = "/ucenik/", method = RequestMethod.GET)
	    public ResponseEntity<List<UcenikEntity>> listAllUsers() {
	        List<UcenikEntity> users = UcenikRepository.findAllUsers();
	        if (users.isEmpty()) {
	            return new ResponseEntity(HttpStatus.NO_CONTENT);
	            // You many decide to return HttpStatus.NOT_FOUND
	        }
	        return new ResponseEntity<List<UcenikEntity>>(users, HttpStatus.OK);
	    }
	 
	    // -------------------Retrieve Single User------------------------------------------
	 
	    @RequestMapping(value = "/ucenik/{id}", method = RequestMethod.GET)
	    public ResponseEntity<?> getUser(@PathVariable("id") long id) {
	        logger.info("Fetching User with id {}", id);
	        UcenikEntity ucenik = UcenikRepository.findById(id);
	        if (ucenik == null) {
	            logger.error("User with id {} not found.", id);
	            return new ResponseEntity(new CustomErrorType("User with id " + id 
	                    + " not found"), HttpStatus.NOT_FOUND);
	        }
	        return new ResponseEntity<UcenikEntity>(ucenik, HttpStatus.OK);
	    }
	 
	    // -------------------Create a User-------------------------------------------
	 
	    @RequestMapping(value = "/ucenik/", method = RequestMethod.POST)
	    public ResponseEntity<?> createUser(@RequestBody UcenikEntity ucenik, UriComponentsBuilder ucBuilder) {
	        logger.info("Creating UcenikEntity : {}", ucenik);
	 
	        if (UcenikRepository.isUserExist(ucenik)) {
	            logger.error("Unable to create. A User with name {} already exist", ucenik.getIme());
	            return new ResponseEntity(new CustomErrorType("Unable to create. A User with name " + 
	            ucenik.getIme() + " already exist."),HttpStatus.CONFLICT);
	        }
	        UcenikRepository.saveUser(ucenik);
	 
	        HttpHeaders headers = new HttpHeaders();
	        headers.setLocation(ucBuilder.path("/api/user/{id}").buildAndExpand(ucenik.getId()).toUri());
	        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	    }
	 
	    // ------------------- Update a User ------------------------------------------------
	 
	    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
	    public ResponseEntity<?> updateUser(@PathVariable("id") long id, @RequestBody UcenikEntity ucenik) {
	        logger.info("Updating User with id {}", id);
	 
	        UcenikEntity currentUcenik = UcenikRepository.findById(id);
	 
	        if (currentUcenik == null) {
	            logger.error("Unable to update. User with id {} not found.", id);
	            return new ResponseEntity(new CustomErrorType("Unable to upate. User with id " + id + " not found."),
	                    HttpStatus.NOT_FOUND);
	        }
	 
	        currentUcenik.setJmbg(ucenik.getJmbg());
	        currentUcenik.setIme(ucenik.getIme());
	        currentUcenik.setPrezime(ucenik.getPrezime());
	 
	        UcenikRepository.updateUcenikEntity(currentUcenik);
	        return new ResponseEntity<UcenikEntity>(currentUcenik, HttpStatus.OK);
	    }
	 
	    // ------------------- Delete a User-----------------------------------------
	 
	    @RequestMapping(value = "/ucenik/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> deleteUser(@PathVariable("id") long id) {
	        logger.info("Fetching & Deleting User with id {}", id);
	 
	        UcenikEntity ucenik = UcenikRepository.findById(id);
	        if (ucenik == null) {
	            logger.error("Unable to delete. User with id {} not found.", id);
	            return new ResponseEntity(new CustomErrorType("Unable to delete. User with id " + id + " not found."),
	                    HttpStatus.NOT_FOUND);
	        }
	        UcenikRepository.deleteUserById(id);
	        return new ResponseEntity<UcenikEntity>(HttpStatus.NO_CONTENT);
	    }
	 
	   
	 
	}*/




