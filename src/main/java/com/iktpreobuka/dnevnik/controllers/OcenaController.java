package com.iktpreobuka.dnevnik.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iktpreobuka.dnevnik.entities.OcenaEntity;
import com.iktpreobuka.dnevnik.entities.UcenikEntity;
import com.iktpreobuka.dnevnik.repositories.OcenaRepository;

@RestController
@RequestMapping(path = "/api/v1/ocena")
public class OcenaController {
	
		@Autowired
		private OcenaRepository ocenaRepository;
	
		//metoda post
		@RequestMapping(method = RequestMethod.POST)
		public OcenaEntity addNewUser(@RequestParam Integer vrednostOcene, 
				@RequestParam Integer zakljucnaOcena){
			
			OcenaEntity ocena = new OcenaEntity();
			ocena.setVrednostOcene(vrednostOcene);
			ocena.setZakljucnaOcena(zakljucnaOcena);
			ocenaRepository.save(ocena);
			return ocena;
	}
		
		 @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
		  	public OcenaEntity updateUser(@PathVariable Integer id, @RequestParam Integer vrednostOcene, 
		  			@RequestParam Integer zakljucnaOcena) {
			 	OcenaEntity user = ocenaRepository.findOne(id);
		  		user.setVrednostOcene(vrednostOcene);
		  		user.setZakljucnaOcena(zakljucnaOcena);
		  		
		  		return ocenaRepository.save(user);
		  	}
		//get
		@RequestMapping(method = RequestMethod.GET)
		public Iterable<OcenaEntity> getAllUsers() {
	    	  return ocenaRepository.findAll();
	      }
		 
		
		//delete
		@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
      	public void deleteUser(@PathVariable Integer id){
    	  OcenaEntity user = ocenaRepository.findOne(id);
    	  ocenaRepository.delete(user);
    			  
      }
}
