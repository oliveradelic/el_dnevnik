package com.iktpreobuka.dnevnik.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iktpreobuka.dnevnik.entities.OdeljenjeEntity;
import com.iktpreobuka.dnevnik.entities.UcenikEntity;
import com.iktpreobuka.dnevnik.repositories.OdeljenjeRepository;

@RestController
@RequestMapping(path = "/api/v1/odeljenje")
public class OdeljenjeController {
	
	@Autowired
	private OdeljenjeRepository odeljenjeRepository;
	
	@RequestMapping(method = RequestMethod.POST)
	public OdeljenjeEntity addNewUser(@RequestParam String oznakaOdeljenja){
		
		OdeljenjeEntity odeljenje = new OdeljenjeEntity();
		odeljenje.setOznakaOdeljenja(oznakaOdeljenja);
		odeljenjeRepository.save(odeljenje);
		return odeljenje;
	}
	
	//metoda get
	@RequestMapping(method = RequestMethod.GET)
	public Iterable<OdeljenjeEntity> getAllUsers(){
		return odeljenjeRepository.findAll();
	}
	
	 @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	  	public OdeljenjeEntity updateUser(@PathVariable Integer id, @RequestParam String oznakaOdeljenja) {
		 OdeljenjeEntity user = odeljenjeRepository.findOne(id);
	  		user.setOznakaOdeljenja(oznakaOdeljenja);
	  		return odeljenjeRepository.save(user);
	  	}
	
	//metoda delete
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void deleteUser(@PathVariable Integer id){
		OdeljenjeEntity odeljenje = odeljenjeRepository.findOne(id);
				odeljenjeRepository.delete(odeljenje);
	}
	

}
