package com.iktpreobuka.dnevnik.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iktpreobuka.dnevnik.entities.RazredEntity;
import com.iktpreobuka.dnevnik.entities.UcenikEntity;
import com.iktpreobuka.dnevnik.repositories.RazredRepository;

@RestController
@RequestMapping(path = "/api/v1/razred")
public class RazredController {
	
		@Autowired
		private RazredRepository razredRepository;
		
		@RequestMapping(method = RequestMethod.POST)
		public RazredEntity addNewUser(@RequestParam Integer godinaRaz){
			
			RazredEntity razred = new RazredEntity();
			razred.setGodinaRaz(godinaRaz);
			razredRepository.save(razred);
			return razred;
		}
		
		@RequestMapping(method = RequestMethod.GET)
		public Iterable<RazredEntity> getAllUsers(){
			return razredRepository.findAll();
		}
		
		 @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
		  	public RazredEntity updateUser(@PathVariable Integer id, @RequestParam Integer godinaRaz) {
		  		RazredEntity user = razredRepository.findOne(id);
		  		user.setGodinaRaz(godinaRaz);
		  		return razredRepository.save(user);
		  	}
		
		@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
			public void deleteUser(@PathVariable Integer id){
			RazredEntity razred = razredRepository.findOne(id);
			razredRepository.delete(razred);
					
		}
		
	
	
	

}
