package com.iktpreobuka.dnevnik.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iktpreobuka.dnevnik.entities.RoditeljEntity;
import com.iktpreobuka.dnevnik.entities.UcenikEntity;
import com.iktpreobuka.dnevnik.repositories.RoditeljRepository;
import com.iktpreobuka.dnevnik.repositories.UcenikRepository;

@RestController
@RequestMapping(path = "/api/v1/roditelj")
public class RoditeljController {

	      @Autowired
	      private RoditeljRepository roditeljRepository;
	      
	      @RequestMapping(method = RequestMethod.POST)
	      public RoditeljEntity addNewUser(@RequestParam String ime, 
	    		  			@RequestParam String prezime, 
	    		  			@RequestParam String jmbg) {
	    	  
	    	  RoditeljEntity user = new RoditeljEntity();
	    	  user.setIme(ime);
	    	  user.setPrezime(prezime);
	    	  user.setJmbg(jmbg);
	    	  roditeljRepository.save(user);
	    	  return user;
	      }
	      
	      @RequestMapping(method = RequestMethod.GET)
	       public Iterable<RoditeljEntity> getAllUsers() {
	    	  return roditeljRepository.findAll();
	      }
	      
	      @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	    	public RoditeljEntity updateUser(@PathVariable Integer id, @RequestBody String ime, @RequestBody String prezime,
	    			@RequestBody String jmbg) {
	    	  RoditeljEntity user = roditeljRepository.findOne(id);
	    		user.setIme(ime);
	    		user.setPrezime(prezime);
	    		user.setJmbg(jmbg);
	    		return roditeljRepository.save(user);
	    	}
	      
	      @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	  		public void deleteUser(@PathVariable Integer id) {
	  		RoditeljEntity user = roditeljRepository.findOne(id);
	  		roditeljRepository.delete(user);
	  	}
	}


