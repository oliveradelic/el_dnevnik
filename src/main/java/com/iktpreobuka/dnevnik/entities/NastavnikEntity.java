package com.iktpreobuka.dnevnik.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class NastavnikEntity extends OsobaEntity {
	
	@Column
	private String jmbg;

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public NastavnikEntity() {
		super();
	
	}
	
	public Integer getId() {
		return id;
	}

	public String getIme() {
		return ime;
	}
	
	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
		
	}
	
	

}
