package com.iktpreobuka.dnevnik.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class OdeljenjeEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(nullable = false)
	String oznakaOdeljenja;

	public String getOznakaOdeljenja() {
		return oznakaOdeljenja;
	}

	public void setOznakaOdeljenja(String oznakaOdeljenja) {
		this.oznakaOdeljenja = oznakaOdeljenja;
	}

	public OdeljenjeEntity() {
		
	}
	
	

}
