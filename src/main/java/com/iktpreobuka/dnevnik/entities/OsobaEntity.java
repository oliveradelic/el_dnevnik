package com.iktpreobuka.dnevnik.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;


@MappedSuperclass
public abstract class OsobaEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (nullable = false)
	protected Integer id;
	@Column (nullable = false)
	protected String ime;
	@Column (nullable = false)
	protected String prezime;
	
	/*@Version //verzija podataka radi resenja ako postoji kolizija kod azuriranja 
	private Integer version;*/
	
	public OsobaEntity() {
	
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	
	
	
	
	

}
