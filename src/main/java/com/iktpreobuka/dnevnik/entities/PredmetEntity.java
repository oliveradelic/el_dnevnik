package com.iktpreobuka.dnevnik.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PredmetEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(nullable = false)
	private String nazivPredmeta;
	
	@Column(nullable = false)
	private Integer nedeljniFond;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNazivPredmeta() {
		return nazivPredmeta;
	}
	public void setNazivPredmeta(String nazivPredmeta) {
		this.nazivPredmeta = nazivPredmeta;
	}
	public int getNedeljniFond() {
		return nedeljniFond;
	}
	public void setNedeljniFond(Integer nedeljniFond) {
		this.nedeljniFond = nedeljniFond;	
	}
	public PredmetEntity() {
	
	}
	

}
