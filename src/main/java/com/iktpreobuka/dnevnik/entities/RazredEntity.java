package com.iktpreobuka.dnevnik.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class RazredEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
		@Column(nullable = false)
	private Integer godinaRaz;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getGodinaRaz() {
		return godinaRaz;
	}
	public void setGodinaRaz(Integer godinaRaz) {
		this.godinaRaz = godinaRaz;
	}
	
	public RazredEntity() {
	
	}
}
