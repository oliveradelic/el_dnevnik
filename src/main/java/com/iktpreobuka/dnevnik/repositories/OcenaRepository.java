package com.iktpreobuka.dnevnik.repositories;

import org.springframework.data.repository.CrudRepository;

import com.iktpreobuka.dnevnik.entities.OcenaEntity;

public interface OcenaRepository extends CrudRepository<OcenaEntity, Integer>{

}
