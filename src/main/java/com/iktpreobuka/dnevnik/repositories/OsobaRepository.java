package com.iktpreobuka.dnevnik.repositories;

import org.springframework.data.repository.CrudRepository;

import com.iktpreobuka.dnevnik.entities.OsobaEntity;

public interface OsobaRepository extends CrudRepository<OsobaEntity, Integer> {

}
